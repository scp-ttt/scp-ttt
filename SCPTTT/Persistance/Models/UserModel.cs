﻿using SCPTTT.Game;

namespace SCPTTT.Persistance.Models
{
    /// <summary>
    /// A model for serializing a <see cref="Game.PlayerInfo"/> to JSON.
    /// </summary>
    public class UserModel
    {

        public string UserId { get; set; }

        public int Kills { get; set; }

        public int Deaths { get; set; }

        public float Karma { get; set; }

        public int Level { get; set; }

        public float Experience { get; set; }

        public UserModel() { }

        public UserModel(PlayerInfo info)
        {
            UserId = info.UserId;
            Kills = info.Kills;
            Deaths = info.Deaths;
            Karma = (info.LiveKarma > ScpTttConstants.KARMA_MAX_VALUE ? ScpTttConstants.KARMA_MAX_VALUE : info.LiveKarma);
            Experience = info.Xp.Xp;
            Level = info.Xp.Level;
        }

    }
}
