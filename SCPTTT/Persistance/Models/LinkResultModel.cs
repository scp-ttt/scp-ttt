﻿namespace SCPTTT.Persistance.Models
{
    public class LinkResultModel
    {

        public bool Success { get; set; }

        public string Code { get; set; }

        public string Message { get; set; }

    }
}
