﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SCPTTT.Game;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCPTTT.Persistance.Models
{
    public class RoundModel
    {

        public IEnumerable<string> Players { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public TttRole? Victor { get; set; }

        public RoundModel(TttRound round)
        {
            Players = ScpTtt.Instance.Players.Keys.AsEnumerable();
            Victor = round.Victor;
            StartTime = round.RoundStartTime;
            EndTime = round.RoundEndTime;
        }

        public RoundModel() { }

    }
}
