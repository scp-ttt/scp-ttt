﻿using Newtonsoft.Json;
using PluginAPI.Core;
using SCPTTT.Persistance.Models;
//using SeqExiledLogs;
using System.Net.Http;
using System.Text;
using PlayerInfo = SCPTTT.Game.PlayerInfo;

namespace SCPTTT.Persistance.Endpoints
{
    public class UserEndpoint
    {
        private readonly HttpClient _client;

        public UserEndpoint(HttpClient client) => _client = client;

        public UserModel GetUser(string userID)
        {
            Log.Info("Getting user info for " + userID);
            var task = _client.GetAsync(ScpTtt.Instance._configTtt.Endpoint
                + "api/player/steam/" + userID);
            task.Wait();
            var getJsonResponse = task.Result;
            if (!getJsonResponse.IsSuccessStatusCode)
            {
                Log.Error("Failed to get user info!");
                //LogToSeq.Error("Failed to get user info for {UserId}.", userID);
                return null;
            }
            string userInfo = getJsonResponse.Content.ReadAsStringAsync().Result;
            UserModel model = JsonConvert.DeserializeObject<UserModel>(userInfo);
            Log.Debug("Retrieved information about user " + userID);
            //LogToSeq.Info("Retrieved user info for {UserId}.", userID);
            return model;
        }

        public void LinkDiscord(PlayerInfo user)
        {
            UserDiscordLinkModel model = new UserDiscordLinkModel()
            {
                UserId = user.UserId
            };
            string json = JsonConvert.SerializeObject(model);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var task = _client.PostAsync(ScpTtt.Instance._configTtt.Endpoint + "api/link/steam",content);
            task.Wait();
            var res = task.Result;
            var str = res.Content.ReadAsStringAsync();
            str.Wait();
            LinkResultModel resultModel = JsonConvert.DeserializeObject<LinkResultModel>(str.Result);
            if (resultModel == null)
            {
                Log.Error("Invalid LinkResultModel returned when getting link send result for" + user.UserId);
                return;
            }
            if (resultModel.Success) {
                Player.Get(user.UserId)?
                    .SendConsoleMessage("Successfully registered link intent. - "+resultModel.Message+" - Use this code to link: "+resultModel.Code,"yellow");
            } else
            {
                Player.Get(user.UserId)?
                    .SendConsoleMessage("Failed to register link intent: " + resultModel.Message + " - If this is unexpected please report this to a server administrator", "red");
            }
        }

        public void StoreUser(PlayerInfo model)
        {
            Log.Info("Storing user info for " + model.UserId);
            if (string.IsNullOrEmpty(model.UserId))
            {
                Log.Error("Invalid player model in StoreUser");
                //LogToSeq.Error("Attempted to store invalid user model.");
                return;
            }
            UserModel m = new UserModel(model);
            string json = JsonConvert.SerializeObject(m);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var task = _client.PutAsync(ScpTtt.Instance._configTtt.Endpoint + "api/player/steam/update/server", content);
            task.Wait(); // fuck this
            var res = task.Result;
            if (res.IsSuccessStatusCode)
            {
                Log.Info("Saved player info for " + model.UserId);
                //LogToSeq.Info("Applied user update for {UserId}.", model.UserId);
            }
            else
            {
                Log.Error("Failed to store player info: " + model.UserId+" - "+res.StatusCode);
                //LogToSeq.Error("Failed to apply user update for {UserId}.", model.UserId);
            }
        }

    }
}
