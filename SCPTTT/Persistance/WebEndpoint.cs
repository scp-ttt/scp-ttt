﻿using SCPTTT.Persistance.Endpoints;
using System.Net.Http;

namespace SCPTTT.Persistance
{
    internal class WebEndpoint
    {

        private readonly HttpClient client;

        public UserEndpoint Users { get; private set; }

        public WebEndpoint()
        {
            client = new HttpClient();
            Users = new UserEndpoint(client);
        }


    }
}
