﻿using CommandSystem;
using RemoteAdmin;
using SCPTTT.Game;
using SCPTTT.Game.Items;
using SCPTTT.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SCPTTT.Commands
{
    [CommandHandler(typeof(ClientCommandHandler))]
    public class ShopCommand : ICommand
    {
        public string Command { get; } = "shop";

        public string[] Aliases { get; } = new[] { "traitorshop", "detectiveshop", "tshop", "dshop" };

        public string Description { get; } = "Opens the shop for Traitors & Detectives";

        public bool Execute(ArraySegment<string> arguments, ICommandSender sender, out string response)
        {
            if (!(sender is PlayerCommandSender playerSender))
            {
                response = "You must be a player to run this command";
                return false;
            }

            ReferenceHub playerHub = playerSender.ReferenceHub;

            if (!ScpTtt.Instance.Players.TryGetValue(playerHub.characterClassManager.UserId, out PlayerInfo player))
            {
                response = "Your player info has not been created or loaded.";
                return false;
            }

            if (ScpTtt.Instance.CurrentRound?.Status != RoundStatus.Ongoing)
            {
                response = "You cannot buy items before the round has begun.";
                return false;
            }

            if (player.IsInnocent() || player.Role == TttRole.Spectator)
            {
                response = "Only traitors or detectives can use the buy menu.";
                return false;
            }
            StringBuilder sb = new StringBuilder();

            sb.AppendLine();
            sb.Append("<color=");
            sb.Append(player.Role.ToColor().AsTMColor());
            sb.Append(">");
            sb.Append(player.Role);
            sb.Append("</color>");
            sb.Append("<color=yellow>");
            sb.AppendLine(" Shop's Items:");
            sb.AppendLine("<color=pumpkin>Use .buy <NUM> to buy items.</color>");
            sb.AppendLine("Your credits: " + player.ShopCredits);
            if (player.ShopCredits < 1)
            {
                sb.AppendLine("<color=#FF0000>You have no credits. You won't be able to buy anything.</color>");
            }

            List<ITttItem> availableItems = BuyMenu.AvailableItems.Where(x => x.RequiredRole.ContainsGameRole(player.Role)).ToList();
            foreach(ITttItem item in availableItems)
            {
                int index = BuyMenu.AvailableItems.IndexOf(item)+1;
                sb.Append("#");
                sb.Append(index);
                sb.Append(": ");
                sb.AppendLine(item.Name);
            }
            sb.Append("</color>");
            response = sb.ToString();
            return true;
        }
    }
}
