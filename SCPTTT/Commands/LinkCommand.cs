﻿using CommandSystem;
using RemoteAdmin;
using SCPTTT.Game;
using System;

namespace SCPTTT.Commands
{
    [CommandHandler(typeof(ClientCommandHandler))]
    public class LinkCommand : ICommand
    {
        public string Command => "link";

        public string[] Aliases =>  null;

        public string Description => "Link in-game account to discord.";

        public bool Execute(ArraySegment<string> arguments, ICommandSender sender, out string response)
        {
            if (!(sender is PlayerCommandSender plySender))
            {
                response = "You must be a player to run this command";
                return false;
            }

            if (!ScpTtt.Instance.Players.TryGetValue(plySender.SenderId, out PlayerInfo info))
            {
                response = "You currently have no user info loaded. Please load first.";
                return false;
            }
            response = "Linking...";
            ScpTtt.Instance.Api.Users.LinkDiscord(info);
            return true;
        }
    }
}
