﻿using CommandSystem;
using RemoteAdmin;
using SCPTTT.Util;
using System;
using System.Text;

namespace SCPTTT.Commands
{
    [CommandHandler(typeof(RemoteAdminCommandHandler))]
    public class HintDebugCommand : ICommand
    {
        public string Command => "hint";

        public string[] Aliases => null;

        public string Description => "androx die";

        public bool Execute(ArraySegment<string> arguments, ICommandSender sender, out string response)
        {
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < arguments.Count; i++)
            {
                sb.Append(arguments.At(i));
            }

            PlayerCommandSender pcs = sender as PlayerCommandSender;

            ReferenceHub playerHub = pcs.ReferenceHub;
            sb.Replace("<nl>", "\n");
            playerHub.hints.Show(HintUtilities.CreateTextHint(sb.ToString()));

            response = "Showing";
            return true;
        }
    }
}
