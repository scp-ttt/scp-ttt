﻿using CommandSystem;
using InventorySystem;
using PluginAPI.Core;
using RemoteAdmin;
using SCPTTT.Game;
using SCPTTT.Game.Items;
using SCPTTT.Util;
using System;

namespace SCPTTT.Commands
{
    [CommandHandler(typeof(ClientCommandHandler))]
    public class BuyCommand : ICommand
    {
        public string Command { get; } = "buy";

        public string[] Aliases { get; } = null;

        public string Description { get; } = "Allows for buying items for SCP-TTT Traitors or Detectives";

        public bool Execute(ArraySegment<string> arguments, ICommandSender sender, out string response)
        {
            if (!(sender is PlayerCommandSender playerSender))
            {
                response = "You must be a player to run this command";
                return false;
            }

            ReferenceHub playerHub = playerSender.ReferenceHub;

            if (!ScpTtt.Instance.Players.TryGetValue(playerHub.characterClassManager.UserId, out Game.PlayerInfo player))
            {
                response = "Your player info has not been created or loaded.";
                return false;
            }

            if (ScpTtt.Instance.CurrentRound?.Status != RoundStatus.Ongoing)
            {
                response = "You cannot buy items before the round has begun.";
                return false;
            }

            if (player.IsInnocent())
            {
                response = "Only traitors or detectives can use the buy menu.";
                return false;
            }

            if (arguments.Count != 1)
            {
                response = "Invalid amount of arguments provided. \n.buy <itemid>";
                return false;
            }

            if (!int.TryParse(arguments.At(0), out int itemId))
            {
                response = "Unknown item id " + arguments.At(0)+" - Do .shop to view available items";
                return false;
            }

            if (!BuyMenu.AvailableItems.TryGet(itemId-1, out ITttItem item))
            {
                response = "Cannot find item at #" + itemId;
                return false;
            }

            if (!item.RequiredRole.ContainsGameRole(player.Role))
            {
                response = "You cannot buy that item!";
                return false;
            }

            if (player.ShopCredits < 1)
            {
                response = "You do not have enough Shop Credits";
                return false;
            }

            player.Hud.ShowNote($"You have bought a {item.Name}.");
            for (int i = 0; i < item.Quanitity; i++)
            {
                if (item.Item.IsArmor())
                {
                    playerHub.inventory.ReplaceArmor(item.Item);
                }
                else
                {
                    playerHub.inventory.ServerAddItem(item.Item);
                }
            }
            Log.Info($"Player {player.UserId} has bought a(n) {item.Name}");
            //LogToSeq.Info("Player {UserId} has just bought x{Quantity} {ItemName} from the {Role} shop.", player.UserId, item.Quanitity, item.Name, player.Role);
            response = $"{item.Name} x{item.Quanitity} bought!";
            ScpTtt.Instance.Players[player.UserId].ShopCredits -= 1;
            return true;

        }
    }
}
