﻿using CommandSystem;
using PluginAPI.Core;
using SCPTTT.Game;
using System;
using System.Text;

namespace SCPTTT.Commands
{
    [CommandHandler(typeof(RemoteAdminCommandHandler))]
    public class PlayerInfoCommand : ICommand
    {
        public string Command { get; } = "stats";

        public string[] Aliases { get; } = new[] { "pinfo" };

        public string Description { get; } = "Gets player stats.";

        public bool Execute(ArraySegment<string> arguments, ICommandSender sender, out string response)
        {
            string pIdStr = arguments.At(0);
            if (!int.TryParse(pIdStr, out int pId))
            {
                response = "Invalid PlayerId specified";
                return false;
            }

            Player ply = Player.Get(pId);
            if (!ScpTtt.Instance.Players.TryGetValue(ply.UserId, out Game.PlayerInfo plyInfo))
            {
                response = $"Player {ply.UserId} is not verified or is not loaded yet.";
                return false;
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine($"UserId: {plyInfo.UserId}");
            sb.AppendLine($"Karma: {plyInfo.Karma} Saved Karma, {plyInfo.Karma} Live Karma, Clean Round? {(plyInfo.CleanRound ? "Yes" : "No")}");
            sb.AppendLine($"K/D: {plyInfo.Kills} Kills, {plyInfo.Deaths} Deaths");
            sb.Append($"Role: Currently {plyInfo.Role}");
            if (plyInfo.Role == TttRole.Spectator && plyInfo.LastRole != TttRole.Spectator)
            {
                sb.Append($", Previously {plyInfo.LastRole}");
            }
            sb.AppendLine();
            sb.AppendLine($"XP: Level {plyInfo.Xp.Level}, {plyInfo.Xp.Xp}xp");
            response = sb.ToString();
            return true;
        }
    }
}
