﻿
using PluginAPI.Core;

namespace SCPTTT.Game
{
    public class Experience
    {
        private readonly PlayerInfo _player;

        public Experience(PlayerInfo player)
        {
            _player = player;
            Level = 1;
            Xp = 0;
        }

        public Experience(PlayerInfo player, int level, float experience)
        {
            _player = player;
            Level = level;
            Xp = experience;
        }

        public int Level { get; private set; }

        public float Xp { get; private set; }

        public void AddXp(float xpToAdd, string msg)
        {
            if (Player.Get(_player.UserId).DoNotTrack)
            {
                // Do not track level or xp changes for DNT users.
                return;
            }
            Xp += xpToAdd;
            string levelupmsg = "";
            if (CheckLevelUp(Level, Xp, out float leftoverXp))
            {
                Level++;
                Xp = leftoverXp;
                levelupmsg = $"| Levelled up! Now Lvl {Level}";
            }
            _player.Hud.ShowNote($"{msg} | +{xpToAdd}xp {levelupmsg}",3);
        }

        private bool CheckLevelUp(int currentLevel, float experience, out float leftoverExperience)
        {
            leftoverExperience = 0;
            float xpToNext = (150 * (currentLevel));
            if (experience >= xpToNext)
            {
                leftoverExperience = (float)(experience - xpToNext);
                return true;
            }
            return false;
        }
    }
}