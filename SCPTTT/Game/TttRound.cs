using InventorySystem.Items;
using MEC;
using PlayerRoles;
using PluginAPI.Core;
using SCPTTT.Util;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SCPTTT.Game
{
    public class TttRound
    {

        public RoundStatus Status { get; private set; }

        public DateTime RoundStartTime { get; private set; }
        public DateTime RoundEndTime { get; private set; }

        private int _detectiveCount;
        private int _innocentCount;
        private int _traitorCount;

        public TttRole? Victor { get; private set; }

        private readonly ScpTtt _instance;

        private float _timer;
        private Action _timerAction;

        public int Timer
        {
            get
            {
                return Mathf.FloorToInt(_timer);
            }
        }

        public TttRound(ScpTtt instance)
        {
            _instance = instance;
        }

        public void ApplyRound()
        {
            RoundStartTime = DateTime.UtcNow;
            Log.Debug("Assigning player roles");
            RollPlayerRoles();
            Log.Debug("Applying player roles");
            foreach (var record in _instance.Players)
            {
                var player = record.Key.ReferenceHub();
                if (player == null)
                    continue;
                Player p = new Player(player);
                p.SetRole(RoleTypeId.Scientist);
                PlayerListHelper.UpdatePlayerIdentity(record.Value, p);
                Timing.RunCoroutine(GiveItems(p));
            }
            PreRound();
        }

        private IEnumerator<float> GiveItems(Player p)
        {
            yield return Timing.WaitForSeconds(0.5f);
            p.AddItem(ItemType.Radio);
            p.AddItem(ItemType.ArmorLight);
        }

        internal void Tick()
        {
            _timer -= 1;
            if (_timer <= 0 && _timerAction != null)
            {
                // This is fucky. It works?
                Action a = _timerAction;
                _timerAction = null;
                a.Invoke();
            }
        }

        private void PreRound()
        {
            _timer = ScpTttConstants.PRE_ROUND_TIMER;
            _timerAction = StartRound;
        }

        private void PostRound()
        {
            _timer = ScpTttConstants.POST_ROUND_TIMER;
            _timerAction = ScpTtt.Instance.EndRound;
        }

        private void StartRound()
        {
            foreach (var info in _instance.Players)
            {
                ReferenceHub infoHub = info.Key.ReferenceHub();
                if (infoHub == null)
                    continue;
                Player p = new Player(infoHub);
                PlayerInfo playerInfo = info.Value;
                if (!playerInfo.IsInnocent())
                {
                    playerInfo.ShopCredits = ScpTttConstants.STARTING_SHOP_CREDITS; // Start with 2 credits.
                }
                if (playerInfo.IsTraitor())
                {
                    p.ReferenceHub.inventory.ReplaceArmor(ItemType.ArmorCombat);
                    p.AddItem(ItemType.KeycardZoneManager);
                }
                if (playerInfo.IsDetective())
                {
                    Timing.RunCoroutine(SpawnDetective(p));
                }
                info.Value.Hud.ShowNote($"Your karma is {Karma.StringFormatValue(info.Value.Karma, false)}, you deal {Karma.ScaleDamage(100,info.Value.Karma):F2}% damage this round.", 10);
                info.Value.UpdateCustomInfo();
                infoHub.nicknameSync.NetworkViewRange = 100;
            }
            Status = RoundStatus.Ongoing;
            _timer = ScpTttConstants.ROUND_DURATION_TIMER;
            _timerAction = Timeout;
            Log.Info("Round started!");
            //LogToSeq.Info("Round has started. There are {InnocentCount} innocents, {TraitorCount} traitors and {DetectiveCount} detectives.",_innocentCount,_traitorCount,_detectiveCount);
            _instance.CheckForRoundEnd(); // fuck you masonic
        }

        private IEnumerator<float> SpawnDetective(Player p)
        {
            Vector3 pos = p.Position;
            p.SetRole(RoleTypeId.FacilityGuard);
            yield return Timing.WaitForOneFrame;
            p.Position = pos;
            yield return Timing.WaitForOneFrame;
            foreach (ItemBase item in p.Items)
            {
                p.AddItem(item.ItemTypeId);
                yield return Timing.WaitForOneFrame;
            }
            p.ReferenceHub.inventory.ReplaceArmor(ItemType.ArmorCombat);
        }

        private void Timeout()
        {
            Log.Info("Time out! Innocents win!");
            Victor = TttRole.Innocent;
            End();
        }

        private void RollPlayerRoles()
        {
            Log.Debug("Rolling player roles.");
            Stack<Player> playerStack = new Stack<Player>(Player.GetPlayers().Shuffle());
            int playerCount = playerStack.Count;

            int traitors = (playerCount <= 4 ? 1 : 1+(int)Mathf.Floor(playerCount / 4));
            int detectives = (int)Mathf.Floor(playerCount / 8);
            Log.Debug("Rolled! Traitors: " + traitors + " - Detectives: " + detectives+" - Innocents: "+(playerCount - (traitors + detectives))+".");
            _traitorCount = traitors;
            _detectiveCount = detectives;
            _innocentCount = 0;

            for (int i = 0; i < traitors; i++)
            {
                Player ply = playerStack.Pop();
                _instance.UpdatePlayerRole(ply, TttRole.Traitor);
            }

            for (int i = 0; i < detectives; i++)
            {
                Player ply = playerStack.Pop();
                _instance.UpdatePlayerRole(ply, TttRole.Detective);
            }

            while (!playerStack.IsEmpty())
            {
                Player ply = playerStack.Pop();
                _instance.UpdatePlayerRole(ply, TttRole.Innocent);
                _innocentCount++;
            }
        }

        public TttRole FindVictor()
        {
            bool allTraitorsDead = true;
            bool noInnocentAlive = true;
            foreach(var record in _instance.Players)
            {
                if (record.Value.Role == TttRole.Traitor)
                    allTraitorsDead = false;
                if (record.Value.Role == TttRole.Detective || record.Value.Role == TttRole.Innocent)
                    noInnocentAlive = false;
            }
            Log.Debug("All traitors dead?: " + allTraitorsDead + " - No innocent alive?: " + noInnocentAlive);
            TttRole victor = noInnocentAlive ? TttRole.Traitor : (allTraitorsDead ? TttRole.Innocent : TttRole.Spectator);
            if (victor == TttRole.Spectator)
            {
                Victor = null; // Conditional operators are poo.
            }
            else
            {
                Victor = victor;
            }
            return victor;
        }

        public void End()
        {
            Status = RoundStatus.PostRound;
            Log.Debug("Round ending timer now.");
            //LogToSeq.Info("Round has ended! The victor is {Victor}! There were {PlayerCount} players this round.", Victor, (_detectiveCount+_innocentCount+_traitorCount));
            RoundEndTime = DateTime.UtcNow;
            if (Victor == TttRole.Innocent) {
                Cassie.Message("INTRUDER SQUAD NEUTRALIZED BY SECURITY PERSONNEL");
            } else
            {
                Cassie.Message("SCIENCE PERSONNEL LOST TO INTRUDER INSURGENCY");
            }
            foreach(var playerKeyValue in ScpTtt.Instance.Players)
            {
                PlayerInfo info = playerKeyValue.Value;
                if ((info.Role == Victor || info.LastRole == Victor))
                {
                    info.Xp.AddXp(ScpTttConstants.XP_ROUND_VICTOR, ScpTttConstants.MESSAGE_ROUND_VICTOR);
                } else
                {
                    info.Xp.AddXp(ScpTttConstants.XP_ROUND_LOSER, ScpTttConstants.MESSAGE_ROUND_LOSER);
                }
            }
            PostRound();
        }
    }
}
