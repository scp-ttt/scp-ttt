﻿using InventorySystem.Items.Pickups;
using MapGeneration;
using PluginAPI.Core;
using SCPTTT.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SCPTTT.Game
{
    public static class GunSpawner
    {

        internal static readonly System.Random Random = new System.Random((int)DateTimeOffset.Now.ToUnixTimeMilliseconds());

        internal static bool Ran { get; private set; } = false;

        private static readonly Dictionary<ItemType, ItemType> spawnableGuns = new Dictionary<ItemType, ItemType>
        {
            { ItemType.GunCOM15, ItemType.Ammo9x19 },
            { ItemType.GunE11SR, ItemType.Ammo556x45 },
            { ItemType.GunFSP9, ItemType.Ammo9x19 },
            { ItemType.GunCOM18, ItemType.Ammo9x19 },
            { ItemType.GunCrossvec, ItemType.Ammo9x19 },
            { ItemType.GunShotgun, ItemType.Ammo12gauge },
            { ItemType.GunAK, ItemType.Ammo762x39 },
        };

        private static readonly List<RoomName> blacklistedRooms = new List<RoomName>
        {
            RoomName.Lcz173,
            RoomName.LczCheckpointA,
            RoomName.LczCheckpointB
        };

        public static void RoundEnded()
        {
            Ran = false;
        }

        internal static List<Vector3> GunSpawnLocations;

        public static void Spawn()
        {
            if (Ran)
            {
                Log.Error("Gun spawning has already occured.");
                return;
            }
            Ran = true;

            GunSpawnLocations = new List<Vector3>();
            ItemPickupBase[] pickups = UnityEngine.Object.FindObjectsOfType<ItemPickupBase>();
            foreach (var pickup in pickups)
            {
                pickup.DestroySelf(); // Destroy them all. No more guns :)
            }

            List<Vector3> rooms = GatherRooms();
            rooms.ShuffleList();

            Queue<ItemType> gunQueue = new Queue<ItemType>();
            for (int i = 0; i < rooms.Count; i++)
            {
                gunQueue.Enqueue(RollItem());
            }

            foreach (var room in rooms)
            {
                Vector3 pos = room + new Vector3(0, 2, 0);
                GunSpawnLocations.Add(pos);
                ItemType spawnedGun = gunQueue.Dequeue();
                PickupFactory.GeneratePickup(spawnedGun, RandomOffset(pos));
                PickupFactory.GenerateMultiple(spawnableGuns[spawnedGun], RandomOffset(pos), 5);
            }
            Log.Info("GunSpawner - Done :)");
        }

        private static Vector3 RandomOffset(Vector3 pos, float range = 0.5f)
        {
            int r = (int) (range * 100);
            int x = Random.Next(-r, r);
            int z = Random.Next(-r, r);
            return pos + new Vector3(((float) x)/100, 0, ((float)z) / 100);
        }

        private static ItemType RollItem()
        {
            int r = Random.Next(0, spawnableGuns.Count);
            ItemType type = spawnableGuns.Keys.ToList()[r];
            return type;
        }

        private static List<Vector3> GatherRooms()
        {
            List<Vector3> roomPositions = new List<Vector3>();
            foreach(RoomIdentifier room in Map.Rooms)
            {
                if (room.Zone != FacilityZone.LightContainment)
                    continue;

                if (blacklistedRooms.Contains(room.Name))
                    continue;

                Vector3 roomPosition = room.ApiRoom.Position;

                Log.Debug($"GunSpawner - Gathered room {room.Name} with pos {roomPosition}");
                roomPositions.Add(roomPosition);
            }
            return roomPositions;
        }
    }
}
