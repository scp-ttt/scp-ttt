﻿using SCPTTT.Game.Items;
using System.Collections.Generic;

namespace SCPTTT.Game
{
    public static class BuyMenu
    {
        /// <summary>
        /// A list of items that are available for purchase via the shop.
        /// 
        /// More items can be made available by simple appending them to this list.
        /// </summary>
        public static readonly List<ITttItem> AvailableItems = new List<ITttItem>()
        {
            // Traitor Items
            new ItemBall(),
            new ItemHat(),
            new ItemNade(),
            new ItemHandCannon(),

            // Detective Items
            new ItemHmg(),
            new ItemMedkit(),
            new ItemFlash(),
            new ItemAdrenaline(),
            new ItemArmor()
        };



    }
}
