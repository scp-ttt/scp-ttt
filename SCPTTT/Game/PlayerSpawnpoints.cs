﻿using UnityEngine;

namespace SCPTTT.Game
{
    public static class PlayerSpawnpoints
    {

        public static Vector3 RandomSpawn => RollSpawn();

        private static Vector3 RollSpawn()
        {
            int index = GunSpawner.Random.Next(0, GunSpawner.GunSpawnLocations.Count);
            Vector3 retTransform = GunSpawner.GunSpawnLocations[index];
            return retTransform;
        }

    }
}
