﻿namespace SCPTTT.Game
{
    /// <summary>
    /// The current status of the round.
    /// </summary>
    public enum RoundStatus
    {

        PreRound,
        Ongoing,
        PostRound

    }
}
