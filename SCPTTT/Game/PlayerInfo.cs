﻿using SCPTTT.Hud;
using SCPTTT.Persistance.Models;

namespace SCPTTT.Game
{
    public class PlayerInfo
    {

        public TttRole Role { get; set; }

        public string UserId { get; set; }

        public float Karma { get; set; }

        public float LiveKarma { get; set; }

        public int Kills { get; set; }

        public int Deaths { get; set; }

        public bool CleanRound { get; set; }

        public int ShopCredits { get; set; }

        public TttRole LastRole { get; set; }

        public bool CorpseReported { get; set; }

        public PlayerHud Hud { get; private set; }

        public Experience Xp { get; private set; }

        private PlayerInfo() { }

        public static PlayerInfo New(string userId)
        {
            PlayerInfo plyInfo = new PlayerInfo()
            {
                Role = TttRole.Spectator,
                UserId = userId,
                Karma = 1000,
                LiveKarma = 1000,
                Kills = 0,
                Deaths = 0,
                CleanRound = true,
                Hud = new PlayerHud(),
            };
            plyInfo.Xp = new Experience(plyInfo);
            return plyInfo;
        }

        public static PlayerInfo FromModel(UserModel model)
        {
            PlayerInfo plyInfo = new PlayerInfo()
            {
                Role = TttRole.Spectator,
                UserId = model.UserId,
                Karma = model.Karma,
                LiveKarma = model.Karma,
                Kills = model.Kills,
                Deaths = model.Deaths,
                CleanRound = true,
                Hud = new PlayerHud()
            };
            plyInfo.Xp = new Experience(plyInfo, model.Level, model.Experience);
            return plyInfo;
        }

        public override string ToString()
        {
            return $"{UserId} / {Role} / {Karma} ({LiveKarma}, {CleanRound}) / {Kills}-{Deaths} / Lvl {Xp.Level}, {Xp.Xp} xp";
        }

    }
}
