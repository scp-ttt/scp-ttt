﻿using SCPTTT.Util;
using UnityEngine;

namespace SCPTTT.Game
{
    public static class Karma
    {

        /// <summary>
        /// The karma loss for damaging someone.
        /// </summary>
        /// <param name="attackerInfo"></param>
        /// <returns>The karma penalty for damaging a friendly player.</returns>
        public static float HurtPenalty(PlayerInfo attackerInfo, float dmg)
        {
            if (!ScpTtt.Instance.Players.ContainsValue(attackerInfo))
                return 0;
            return (attackerInfo.Karma * Mathf.Clamp01(dmg * ScpTttConstants.KARMA_DAMAGE_PENALTY_RATIO));
        }

        /// <summary>
        /// The karma loss for last-hitting someone.
        /// </summary>
        /// <param name="attackerInfo"></param>
        /// <returns>The karma penalty for last-hitting a friendly player.</returns>
        public static float KillPenalty(PlayerInfo attackerInfo)
        {
            return HurtPenalty(attackerInfo, ScpTttConstants.KARMA_KILL_PENALTY);
        }

        public static float KillReward()
        {
            return HurtReward(ScpTttConstants.KARMA_KILL_BONUS);
        }

        public static float HurtReward(float dmg)
        {
            return (ScpTttConstants.KARMA_MAX_VALUE * Mathf.Clamp01(dmg * ScpTttConstants.KARMA_DAMAGE_REWARD_RATIO));
        }

        public static float ScaleDamage(float damage, float karma)
        {
            return damage * Scale(karma);
        }

        public static float Scale(float karma) {
            if (karma >= ScpTttConstants.KARMA_MAX_VALUE)
                return 1f;
            karma -= ScpTttConstants.KARMA_MAX_VALUE; 
            return Mathf.Clamp((1 + -0.0000018f * (karma * karma)), 0.1f, 1);
        }

        public static string StringFormatValue(float karma, bool withWarning = false)
        {
            Color c = Color.white;
            if (karma >= 750)
                c = Color.green;
            else if (karma >= 500)
                c = Color.yellow;
            else if (karma >= 0)
                c = Color.red;

            string warning = "";
            if (karma <= 499)
                warning = "\n<i>Your karma is low. Do not friendly fire.</i>";
            return $"<color={c.AsTMColor()}>{(int)Mathf.Round(karma)}</color>{(withWarning ? warning : "")}";
        }

    }
}
