﻿namespace SCPTTT.Game
{
    /// <summary>
    /// Determines the player's role within the round.
    /// </summary>
    public enum TttRole
    {
        Spectator,
        Innocent,
        Traitor,
        Detective

    }
}
