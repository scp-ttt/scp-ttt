﻿namespace SCPTTT.Game.Items
{
    public class ItemMedkit : ITttItem
    {
        public string Name { get; } = "Medkit";

        public ItemType Item { get; } = ItemType.Medkit;

        public BuyableRole RequiredRole { get; } = BuyableRole.Detective | BuyableRole.Traitor;

        public int Quanitity => 1;
    }
}
