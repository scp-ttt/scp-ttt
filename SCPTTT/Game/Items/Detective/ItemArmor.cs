﻿namespace SCPTTT.Game.Items
{
    public class ItemArmor : ITttItem
    {
        public string Name => "Heavy Armor";

        public ItemType Item => ItemType.ArmorHeavy;

        public BuyableRole RequiredRole => BuyableRole.Detective;

        public int Quanitity => 1;
    }
}
