﻿namespace SCPTTT.Game.Items
{
    public class ItemAdrenaline : ITttItem
    {
        public string Name { get; } = "Adrenaline Boosters";

        public ItemType Item { get; } = ItemType.Adrenaline;

        public BuyableRole RequiredRole { get; } = BuyableRole.Detective;

        public int Quanitity => 2;
    }
}
