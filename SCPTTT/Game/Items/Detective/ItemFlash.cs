﻿namespace SCPTTT.Game.Items
{
    public class ItemFlash : ITttItem
    {
        public string Name { get; } = "Flashbang";

        public ItemType Item { get; } = ItemType.GrenadeFlash;

        public BuyableRole RequiredRole { get; } = BuyableRole.Detective;

        public int Quanitity => 1;
    }
}
