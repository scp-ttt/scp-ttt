﻿namespace SCPTTT.Game.Items
{
    public class ItemHmg : ITttItem
    {
        public string Name { get; } = "Heavy Machine Gun";

        public ItemType Item { get; } = ItemType.GunLogicer;

        public BuyableRole RequiredRole { get; } = BuyableRole.Detective;

        public int Quanitity => 1;
    }
}
