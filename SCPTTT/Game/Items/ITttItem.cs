﻿namespace SCPTTT.Game.Items
{
    /// <summary>
    /// An interface describing items that can be bought via the shop for traitors or detectives.
    /// The item can be restricted to a specific role by setting <see cref="RequiredRole"/>.
    /// </summary>
    public interface ITttItem
    {
        /// <summary>
        /// The display name of the item as visible on the shop.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The <see cref="ItemType"/> that is given to the user's inventory when the item 
        /// is bought from the shop.
        /// </summary>
        ItemType Item { get; }

        /// <summary>
        /// The role that is required to buy this item. This can either be <see cref="TttRole.Detective"/>
        /// or <see cref="TttRole.Traitor"/>.
        /// </summary>
        BuyableRole RequiredRole { get; }

        int Quanitity { get; }
    }
}
