﻿namespace SCPTTT.Game.Items
{
    public class ItemNade : ITttItem
    {
        public string Name { get; } = "Frag Grenade";

        public ItemType Item { get; } = ItemType.GrenadeHE;

        public BuyableRole RequiredRole { get; } = BuyableRole.Traitor;

        public int Quanitity => 1;
    }
}
