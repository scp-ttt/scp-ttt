﻿namespace SCPTTT.Game.Items
{
    public class ItemHat : ITttItem
    {
        public string Name { get; } = "Invisibility Cloak";
        public ItemType Item { get; } = ItemType.SCP268;
        public BuyableRole RequiredRole { get; } = BuyableRole.Traitor;

        public int Quanitity => 1;
    }
}
