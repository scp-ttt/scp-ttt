﻿namespace SCPTTT.Game.Items
{
    public class ItemBall : ITttItem
    {
        public string Name { get; } = "Bouncing Betty";

        public ItemType Item { get; } = ItemType.SCP018;

        public BuyableRole RequiredRole { get; } = BuyableRole.Traitor;

        public int Quanitity => 1;
    }
}
