﻿namespace SCPTTT.Game.Items
{
    public class ItemHandCannon : ITttItem
    {
        public string Name { get; } = "Hand Cannon";

        public ItemType Item { get; } = ItemType.GunRevolver;

        public BuyableRole RequiredRole { get; } = BuyableRole.Traitor;

        public int Quanitity => 1;
    }
}
