﻿using System;

namespace SCPTTT.Game.Items
{
    [Flags]
    public enum BuyableRole
    {
        Innocent = 0,
        Traitor = 1,
        Detective = 1 << 1
    }
}
