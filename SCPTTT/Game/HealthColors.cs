﻿using System.Collections.Generic;

namespace SCPTTT.Game
{
    public static class HealthColors
    {
        private static readonly Dictionary<HealthState, string> healthStates = new Dictionary<HealthState, string>()
        {
            { HealthState.Healthy, "light_green" },
            { HealthState.Hurt, "lime" },
            { HealthState.Wounded, "pumpkin" },
            { HealthState.BadlyWounded, "tomato" },
            { HealthState.NearDeath, "red" },
            { HealthState.Dead, "carmine" }
        };

        /// <summary>
        /// Retrieves a string color value from the <paramref name="health"/> given.
        /// </summary>
        /// <param name="health">Player's health.</param>
        /// <returns>A string color code for the name.</returns>
        public static string GetHealthColor(float health) => 
            healthStates.TryGetValue(StateFromHp(health), out string ret) ? ret : "default"; 

        public static HealthState StateFromHp(float hp)
        {
            if (hp >= 90)
                return HealthState.Healthy;
            if (hp >= 70)
                return HealthState.Hurt;
            if (hp >= 45)
                return HealthState.Wounded;
            if (hp >= 20)
                return HealthState.BadlyWounded;
            if (hp >= 1)
                return HealthState.NearDeath;
            return HealthState.Dead;
        }

        public static string Format(this HealthState state)
        {
            switch(state)
            {
                case HealthState.BadlyWounded:
                    return "Badly Wounded";
                case HealthState.Dead:
                    return "Dead";
                case HealthState.Healthy:
                    return "Healthy";
                case HealthState.Hurt:
                    return "Hurt";
                case HealthState.Wounded:
                    return "Wounded";
                case HealthState.NearDeath:
                    return "Near-Death";
                default:
                    return state.ToString();
            }
        }

    }

    public enum HealthState
    {
        Healthy,
        Hurt,
        Wounded,
        BadlyWounded,
        NearDeath,
        Dead
    }
}
