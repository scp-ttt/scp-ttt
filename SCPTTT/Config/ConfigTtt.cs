
namespace SCPTTT.Config
{
    public class ConfigTtt
    {
        public bool IsEnabled { get; set; } = true;

        public string Endpoint { get; set; } = "http://127.0.0.1:3000/";

        public string Key { get; set; } = "";
    }
}
