using HarmonyLib;
using InventorySystem.Configs;
using MEC;
using PluginAPI.Core;
using PluginAPI.Core.Attributes;
using PluginAPI.Events;
using SCPTTT.Config;
using SCPTTT.Game;
using SCPTTT.Persistance;
using SCPTTT.Persistance.Models;
using SCPTTT.Util;
using System.Collections.Generic;
using UnityEngine;
using PlayerInfo = SCPTTT.Game.PlayerInfo;
using PluginAPI.Enums;
using SCPTTT.Events;

namespace SCPTTT
{

    /// <summary>
    /// SCP-TTT plugin.
    /// </summary>
    public class ScpTtt
    {

        public const string Version = "0.8.0";

        /// <summary>
        /// Singleton instance of the plugin.
        /// </summary>
        public static ScpTtt Instance { get; private set; }

        /// <summary>
        /// Property defining the current <see cref="TttRound"/>.
        /// </summary>
        public TttRound CurrentRound { get; private set; }

        /// <summary>
        /// A reference to the API object which handles calls to the database. 
        /// </summary>
        internal WebEndpoint Api { get; private set; }

        // Player's gameobjects to their PlayerInfo from either newly created or loaded.
        public Dictionary<string, PlayerInfo> Players;

        [PluginConfig]
        public ConfigTtt _configTtt;

        [PluginEntryPoint("SCP-TTT", Version, "TTT in SCPSL", "SirMeepington")]
        public void EnablePlugin()
        {
            Instance = this;
            EventManager.RegisterEvents(this);
            EventManager.RegisterEvents<GameEventHandler>(this);
            Harmony _harmonyInstance = new Harmony("sirmeepington.scpttt");
            _harmonyInstance.PatchAll();
            Log.Info("Loading...");
            Api = new WebEndpoint();
            CustomNetworkManager.UsingCustomGamemode = true;
            StartingInventories.DefinedInventories.Clear();
        }

        //ScpTttEvent.Update += gameEvents.Update;
        //ScpTttEvent.RolesToBePicked += InitTttRound;

        [PluginEvent(ServerEventType.PlayerLeft)]
        public void Leave(PlayerLeftEvent ex)
        {
            RemovePlayer(ex.Player);
            CheckForRoundEnd();
        }

        [PluginEvent(ServerEventType.PlayerJoined)]
        public void LoadUserInfo(PlayerJoinedEvent args)
        {
            UserModel user = Api.Users.GetUser(args.Player.UserId);
            PlayerInfo info;
            if (user != null)
            {
                info = PlayerInfo.FromModel(user);
                Log.Debug("Created PlayerInfo for " + args.Player.UserId + " from stored info");
            }
            else
            {
                info = PlayerInfo.New(args.Player.UserId);
                Log.Debug("Created fresh PlayerInfo for " + args.Player.UserId + ".");
                Api.Users.StoreUser(info);
            }

            if (!Players.ContainsKey(args.Player.UserId))
            {
                Players[args.Player.UserId] = info;
                Log.Info("Loaded user info for " + args.Player);
            }
            if (CurrentRound != null)
                info.Hud.Setup(info, CurrentRound);

            PlayerListHelper.UpdatePlayerIdentity(info, args.Player);
        }

        [PluginEvent(ServerEventType.RoundStart)]
        public void InitTttRound()
        {
            if (CurrentRound != null)
            {
                return;
            }
            CurrentRound = new TttRound(this);
            CurrentRound.ApplyRound();
            foreach (PlayerInfo info in Players.Values)
            {
                if (info.Hud == null)
                    continue;
                info.Hud.Setup(info, CurrentRound);
            }
        }

        /// <summary>
        /// Checks if the current round has a victor and ends the round if it does.
        /// </summary>
        public void CheckForRoundEnd()
        {
            if (CurrentRound == null || CurrentRound.Status != RoundStatus.Ongoing)
                return;

            TttRole victor = CurrentRound.FindVictor();
            if (victor == TttRole.Spectator)
                return;

            // Actually end the round here.
            CurrentRound.End();
        }

        /// <summary>
        /// Logs the finished round and destroys the object.
        /// </summary>
        public void EndRound()
        {
            TttRound lastRound = CurrentRound;
            CurrentRound = null;
            Log.Info("Round restarting...");
            foreach (var player in Players.Values)
            {
                player.Hud.ShouldUpdate = false; // Prevent updating when unnecessary.
                if (player.CleanRound)
                {
                    player.Karma += ScpTttConstants.KARMA_CLEAN_ROUND;
                }
                Api.Users.StoreUser(player);
            }
            foreach (var player in Player.GetPlayers())
            {
                player.ReferenceHub.hints.Show(HintUtilities.CreateTextHint($"Round restarting...{HintUtilities.AlignAtLine(2)}", 0.1f));
            }
            Timing.RunCoroutine(DelayedRoundRestart());
        }

        private IEnumerator<float> DelayedRoundRestart()
        {
            yield return Timing.WaitForSeconds(7.5f);
            GunSpawner.RoundEnded();
            Round.Restart(false);
        }

        [PluginEvent(ServerEventType.PlayerDeath)]
        public void PlayerDead(PlayerDeathEvent ev) => UpdatePlayerRole(ev.Player, TttRole.Spectator);

        [PluginEvent(ServerEventType.PlayerSpawn)]
        public void PlayerSpawned(PlayerSpawnEvent ev)
        {
            Vector3 randomSpawn = PlayerSpawnpoints.RandomSpawn;
            while (!Physics.Raycast(randomSpawn, -Vector3.up, 100, LayerMask.GetMask("Default")))
            {
                randomSpawn = PlayerSpawnpoints.RandomSpawn;
            }
            ev.Player.Position = randomSpawn;
        }

        public void UpdatePlayerRole(Player player, TttRole role)
        {
            if (CurrentRound == null)
                return;

            Log.Debug("Updating role for " + player);
            if (Players.TryGetValue(player.UserId, out var info))
            {
                info.Role = role;
                if (role != TttRole.Spectator)
                    info.LastRole = role;
                Players[player.UserId] = info; // pog fish
                Log.Debug("Updated " + player.Nickname + "'s role to " + role);
            }
            else
            {
                Log.Error("Tried to set role for invalid player: " + player);
            }
        }

        public void RemovePlayer(Player player)
        {
            if (CurrentRound == null)
                return;

            if (Players.TryGetValue(player.UserId, out var playerinfo))
            {
                Log.Debug("Removing player " + player.UserId);
                Api.Users.StoreUser(playerinfo);
                UpdatePlayerRole(player, TttRole.Spectator);
                Players.Remove(player.UserId);
            }
        }
    }
}
