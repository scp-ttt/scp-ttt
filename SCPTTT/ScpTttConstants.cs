
namespace SCPTTT
{
    public static class ScpTttConstants
    {


        public const int MINIMUM_PLAYERS = 1;

        public const int MAXIMUM_PLAYERS = 20;

        /// <summary>
        /// The time until a round starts.
        /// </summary>
        public const float PRE_ROUND_TIMER = 25f;

        /// <summary>
        /// The maximum length of a round.
        /// </summary>
        public const float ROUND_DURATION_TIMER = 600f;

        public const float POST_ROUND_TIMER = 10f;

        public const float KARMA_KILL_BONUS = 40f;

        public const float KARMA_KILL_PENALTY = 15f;

        public const float KARMA_DAMAGE_PENALTY_RATIO = 0.001f;

        public const float KARMA_DAMAGE_REWARD_RATIO = 0.0003f;

        public const float KARMA_CLEAN_ROUND = 10f;

        public const float KARMA_MAX_VALUE = 1000f;

        public const int STARTING_SHOP_CREDITS = 2;

        public const int TRAITOR_RESULT_SHOW_TIME = 5;

        public const string MESSAGE_TRAITOR_KILL_INNOCENT = "Killed an Innocent as a Traitor";

        public const string MESSAGE_TRAITOR_KILL_DETECTIVE = "Killed a Detective as a Traitor";

        public const float XP_TRAITOR_KILL_INNOCENT = 10F;

        public const float XP_TRAITOR_KILL_DETECTIVE = 30f;

        public const float XP_ROUND_VICTOR = 50f;

        public const float XP_ROUND_LOSER = 10f;

        public const string MESSAGE_ROUND_VICTOR = "End the round on the winning team.";

        public const string MESSAGE_ROUND_LOSER = "End the round on the losing team.";

    }
}
