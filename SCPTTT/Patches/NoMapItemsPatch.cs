﻿using HarmonyLib;
using MapGeneration.Distributors;

namespace SCPTTT.Patches
{
    [HarmonyPatch(typeof(Locker))]
    [HarmonyPatch("FillChamber")]
    public class LockerSpawnItemPatch
    {
        static bool Prefix()
        {
            return false;
        }
    }
}

