﻿using PluginAPI.Core;
using System;

namespace SCPTTT.Events
{
    public static class ScpTttEventExtensions
    {

        public static void InvokeSafely(this ScpTttEventHandler ev)
        {
            if (ev == null)
                return;

            string eventName = ev.GetType().FullName;
            foreach (ScpTttEventHandler handler in ev.GetInvocationList())
            {
                try
                {
                    handler();
                }
                catch (Exception ex)
                {
                    Log.Error($"{ex.Message}: {handler.Method.Name} ({handler.Method.ReflectedType?.FullName}) at {eventName}.");
                    Log.Error(ex.StackTrace);
                }
            }
        }
    }
}
