﻿namespace SCPTTT.Events
{
    public class ScpTttEvent
    {

        public static event ScpTttEventHandler Update;

        public static void OnUpdate() => Update.InvokeSafely();

    }
}
