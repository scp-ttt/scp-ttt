using Hints;
using Interactables.Interobjects;
using Interactables.Interobjects.DoorUtils;
using InventorySystem.Items.Pickups;
using MapGeneration;
using NorthwoodLib;
using PlayerRoles;
using PlayerStatsSystem;
using PluginAPI.Core;
using PluginAPI.Core.Attributes;
using PluginAPI.Core.Zones;
using PluginAPI.Enums;
using PluginAPI.Events;
using SCPTTT.Game;
using SCPTTT.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using PlayerInfo = SCPTTT.Game.PlayerInfo;

namespace SCPTTT.Events
{
    /// <summary>
    /// A handler class for game-specific events.
    /// </summary>
    internal class GameEventHandler
    {

        public ScpTtt Instance { get; internal set; }

        private float _secondTimer;

        public GameEventHandler()
        {
            Instance = ScpTtt.Instance;

            ScpTttEvent.Update += Update;
        }

        ~GameEventHandler()
        {
            ScpTttEvent.Update -= Update;
        }

        /// <summary>
        /// Prepares the game in the lobby stage by locking the round before it starts.
        /// </summary>
        [PluginEvent(ServerEventType.WaitingForPlayers)]
        public void PrepareLobby()
        {
            Server.Instance.GameObject.AddComponent<TttBehaviour>();
            Instance.Players = new Dictionary<string, PlayerInfo>();
            Round.IsLocked = true;
            foreach(ItemPickupBase item in UnityEngine.Object.FindObjectsOfType(typeof(ItemPickupBase)))
            {
                item.DestroySelf();
            }
            GunSpawner.Spawn();
            // Spawn guns
            // Open and lock special doors.
            foreach (DoorNametagExtension door in UnityEngine.Object.FindObjectsOfType<DoorNametagExtension>())
            {
                if (door.GetName.Contains("CHECKPOINT",StringComparison.OrdinalIgnoreCase) || !door.TryGetComponent(out DoorVariant variant))
                {
                    continue;
                }
                // Lock names doors except checkpoints.
                variant.TargetState = true;
                variant.ServerChangeLock(DoorLockReason.AdminCommand,true);
            }
        }

        [PluginEvent(ServerEventType.PlayerChangeRole)]
        public void ResetInv(PlayerChangeRoleEvent ev)
        {
            ev.Player.ClearInventory(); // Remove all items.
        }

        [PluginEvent(ServerEventType.PlayerUsedItem)]
        public void ItemUsedSuccessfully(PlayerUsedItemEvent ev)
        {
            if (Instance.Players.TryGetValue(ev.Player.UserId, out Game.PlayerInfo player))
            {
                player.UpdateCustomInfo();
            }
        }

        /// <summary>
        /// Shows a hint to the player specified in <paramref name="args"/>.
        /// <para/>
        /// This does run if the player has DNT enabled since they should be kicked.
        /// </summary>
        /// <param name="args">The arguments including the <see cref="Player"/></param>
        [PluginEvent(ServerEventType.PlayerJoined)]
        public void AnnounceWelcome(PlayerJoinedEvent args)
        {
            string info = $"<b>Welcome to <color=#ff0000>Trouble in Site-02 / SCP-TTT</color> v{ScpTtt.Version}</b>";
            if (args.Player.DoNotTrack)
            {
                info += "\n<color=yellow>Please note you have DNT (Do Not Track) enabled.</color>";
                info += "\n<color=red>Some systems have been disabled: Kills, Deaths, XP and Level systems.</color>";
                info += "\n<color=green>Please consider turning off DNT in the main menu for the optimal experience.</color>";
            }
            info += HintUtilities.AlignAtLine(args.Player.DoNotTrack ? 3 : 1);
            args.Player.ReferenceHub.hints.Show(HintUtilities.CreateTextHint(info));
        }

        /// <summary>
        /// Runs <see cref="ScpTtt.CheckForRoundEnd"/>.
        /// </summary>
        public void CheckRoundStatus()
        {
            Instance.CheckForRoundEnd();
        }

        public void Update()
        {
            _secondTimer += Time.deltaTime;
            if (_secondTimer > 1)
            {
                _secondTimer = 0;
                if (Instance.CurrentRound != null)
                {
                    Instance.CurrentRound.Tick();
                }
                //--------------------------------------------------
                // Do all second-based operations that work on the user hints
                // between here.

                CheckRadar();
                //--------------------------------------------------

                UpdateHint();
                if (Instance.Players != null)
                {
                    foreach (PlayerInfo info in Instance.Players.Values)
                    {
                        if (info.Hud != null)
                        {
                            info.Hud.TickTimer();
                        }
                    }
                }
            }
            PlayerRaycasts();
        }

        private void CheckRadar()
        {
            if (Instance.CurrentRound == null || Instance.CurrentRound.Status != RoundStatus.Ongoing)
                return;

            foreach(Player p in Player.GetPlayers())
            {
                if (string.IsNullOrEmpty(p.UserId))
                    continue;
                if (!Instance.Players.TryGetValue(p.UserId, out PlayerInfo info))
                    continue;

                if (p.CurrentItem.ItemTypeId == ItemType.Coin)
                {
                    UpdateRadar(p, info);
                }
            }
        }
        
        private void UpdateRadar(Player player, PlayerInfo info)
        {
            if (!FindNearestPlayer(player, out Player nearest, out int distance))
            {
                info.Hud.ShowNote($"No nearby players...", 1);
            }   
            else
            {
                info.Hud.ShowNote($"Nearest Player: {nearest.DisplayNickname} / ~{distance}m...", 1);
            }
        }

        private bool FindNearestPlayer(Player player, out Player nearestPly, out int distance)
        {
            float nearest = float.MaxValue;
            nearestPly = null;
            foreach(Player ply in Player.GetPlayers())
            {
                if (ply == player || (ply.Role != RoleTypeId.Scientist && ply.Role != RoleTypeId.FacilityGuard))
                {
                    continue; // dead or us?
                }
                float nextDist = Vector3.Distance(player.GameObject.transform.position, ply.GameObject.transform.position);
                if (nextDist < nearest)
                {
                    nearest = nextDist;
                    nearestPly = ply;
                }
            }
            distance = UnityEngine.Mathf.RoundToInt(nearest);
            return nearestPly != null;
        }

        private void PlayerRaycasts()
        {
            foreach(var player in Instance.Players)
            {
                if (Instance.Players.Values == null)
                    continue;
                if (player.Value.Role == TttRole.Spectator)
                    continue;

                ReferenceHub hub = player.Key.ReferenceHub();
                if (hub == null)
                    continue;

                Player ply = new Player(hub);
                var inspectionMask = LayerMask.GetMask("Default", "Door", "Ragdoll", "Hitbox");
                if (!Physics.Raycast(new Ray(hub.PlayerCameraReference.position, hub.PlayerCameraReference.forward), out RaycastHit hit, 3, inspectionMask))
                    continue;

                if (hit.collider == null)
                    continue;

                BasicRagdoll ragdoll = hit.collider.GetComponentInParent<BasicRagdoll>();
                if (ragdoll == null) 
                    continue;

                ReferenceHub ownerHub = ragdoll.Info.OwnerHub;
                if (ownerHub == null)
                    continue;

                Player owner = new Player(ownerHub);
                if (!Instance.Players.TryGetValue(owner.UserId, out PlayerInfo deadPlayer) || deadPlayer.Role != TttRole.Spectator)
                    continue;

                if (deadPlayer.CorpseReported)
                    continue;

                owner.DisplayNickname = $"{owner.Nickname} ({deadPlayer.LastRole})";

                if (!Instance.Players.TryGetValue(ply.UserId, out PlayerInfo reporterInfo))
                    return;

                Log.Info($"{ply.Nickname} has discovered {owner.DisplayNickname}'s body!");
                //LogToSeq.Info("Player {FinderName} ({FinderUserId}), a(n) {FinderRole}, has discovered the body of {DeceasedName} ({DeceasedUserId}). They were a {DeceasedRole}."
                //    , ply.Nickname, ply.UserId, reporterInfo.Role, owner.Nickname, owner.UserId, deadPlayer.LastRole);

                if (player.Value.IsDetective())
                {
                    foreach(PlayerInfo p in Instance.Players.Values)
                    {
                        p.Hud.ShowNote($"Detective {ply.Nickname} has discovered {owner.DisplayNickname}'s body; they were a {deadPlayer.LastRole}!",4);
                    }
                }

                deadPlayer.CorpseReported = true;
            }
        }

        private void UpdateHint()
        {
            foreach (var entry in Instance.Players)
            {
                if (entry.Value == null)
                    continue; // No playerinfo yet
                string text = entry.Value.Hud.DisplayString;
                if (string.IsNullOrEmpty(text))
                    continue;
                Hint h = new TextHint(text, new HintParameter[] { new StringHintParameter("") }, HintEffectPresets.FadeInAndOut(0, 0f, 0f), 1);
                ReferenceHub hub = entry.Value.UserId.ReferenceHub();
                if (hub == null)
                    continue; // Player offline.
                hub.hints.Show(h);
            }
        }

        [PluginEvent(ServerEventType.TeamRespawnSelected)]
        public bool PreventRespawn(TeamRespawnSelectedEvent ev)
        {
            return false;
        }

        [PluginEvent(ServerEventType.PlayerInteractElevator)]
        public bool ElevatorTeleport(PlayerInteractElevatorEvent ev)
        {
            RoomName destinationName = ev.Elevator.AssignedGroup == ElevatorManager.ElevatorGroup.LczA01 || ev.Elevator.AssignedGroup == ElevatorManager.ElevatorGroup.LczA02 ?
                RoomName.LczCheckpointB : RoomName.LczCheckpointA;
            FacilityRoom teleportRoom = Map.Rooms.FirstOrDefault(x => x.Name == destinationName).ApiRoom;
            if (Instance.Players.TryGetValue(ev.Player.UserId, out PlayerInfo info))
            {
                info.Hud.ShowNote("You have been teleported to the other traitor room.", 5);
                ev.Player.Position = teleportRoom.Transform.TransformPoint(new Vector3(21, 1, 0));
                //LogToSeq.Info("Player {Nickname} ({UserId}) used the traitor room. Arrived in {RoomName}.",
                //    ev.Player.Nickname,ev.Player.UserId,destinationName.ToString());
            }
            return false;
        }

        [PluginEvent(ServerEventType.Scp914ProcessPlayer)]
        public bool UpgradePlayer(Scp914ProcessPlayerEvent args) 
        {
            if (Instance.CurrentRound == null || Instance.CurrentRound.Status != RoundStatus.Ongoing)
            {
                return false;
            }
            Player ply = args.Player;
            if (!Instance.Players.TryGetValue(ply.UserId, out PlayerInfo plyInfo)){
                return false;
            }
            AnnounceTest(plyInfo.IsTraitor());
            return false;
        }

        [PluginEvent(ServerEventType.Scp914UpgradePickup)]
        public bool DenyItemChanges(Scp914UpgradePickupEvent _) => false;

        [PluginEvent(ServerEventType.Scp914UpgradeInventory)]
        public bool DenyInventoryChanges(Scp914UpgradeInventoryEvent _) => false;

        private void AnnounceTest(bool traitor)
        {
            var room914 = Map.Rooms.FirstOrDefault(x => x.Name == RoomName.Lcz914);
            if (room914 == null) 
            {
                Log.Error("914 room not found.");
                return;
            }
            foreach(Player p in Player.GetPlayers())
            {
                if (!RoomIdUtils.IsWithinRoomBoundaries(room914, p.Position))
                {
                    return;
                }
                if (Instance.Players.TryGetValue(p.UserId,out var info))
                {
                    info.Hud.ShowNote($"The room fills up with a <b>" +
                        $"{(traitor ? "<color=#ff0000>red" : "<color=#00ff00>green")}" +
                        $"</color></b> light",
                        ScpTttConstants.TRAITOR_RESULT_SHOW_TIME);
                }
            }
        }

        [PluginEvent(ServerEventType.PlayerDamage)]
        public bool PlayerHurt(PlayerDamageEvent args)
        {
            if (ScpTtt.Instance.CurrentRound != null && ScpTtt.Instance.CurrentRound.Status == RoundStatus.PreRound)
            {
                return false;
            }
            // TODO: Swap these back after the hotfix goes live.
            Player hurter = args.Target;
            Player victim = args.Player;
            if (!Instance.Players.TryGetValue(hurter.UserId, out PlayerInfo attacker) || !Instance.Players.TryGetValue(victim.UserId, out PlayerInfo target))
                return false;
            if (hurter == victim)
                return false;
            float damageToDeal = ApplyHurtKarma(args, hurter, victim, attacker, target);
            victim.Damage(damageToDeal, hurter);
            PlayerListHelper.UpdatePlayerIdentity(target, victim);
            target.UpdateCustomInfo();
            return false;
        }

        private float ApplyHurtKarma(PlayerDamageEvent args, Player hurter, Player victim, PlayerInfo attacker, PlayerInfo target)
        {
            if (Instance.CurrentRound == null || Instance.CurrentRound.Status != RoundStatus.Ongoing)
                return 0f;
            if (!(args.DamageHandler is StandardDamageHandler damageHandler))
            {
                return 0f;
            }
            float damage = Mathf.Min(victim.Health, Karma.ScaleDamage(damageHandler.Damage, attacker.Karma));
            float penalty = Karma.HurtPenalty(attacker, damage);
            float reward = Karma.HurtReward(damage);
            bool friendlyFire = attacker.IsTraitor() == target.IsTraitor();
            bool karmaChanged = friendlyFire || (!attacker.IsTraitor() && target.IsTraitor());
            //LogToSeq.Info(
            //    "Player {AttackerName} ({AttackerUserId}), a(n), {AttackerRole}" +
            //    " hurt {VictimName} ({VictimUserId}), a(n) {VictimRole}" +
            //    " for {Damage} damage. "+(karmaChanged ? "They " + (friendlyFire ? "lost" : "gained") +
            //    " {KarmaChange} karma." : ""),
            //    hurter.Nickname, attacker.UserId, attacker.Role, victim.Nickname, target.UserId, target.Role, (int)damage, friendlyFire ? (int)penalty : (int)reward);

            if (attacker.IsTraitor() == target.IsTraitor()) // No friendly fire :)
            {
                attacker.LiveKarma -= penalty;
                attacker.CleanRound = false;
                Instance.Players[attacker.UserId] = attacker;
                Log.Debug(attacker.UserId + " attacked " + target.UserId + " for " + damage + " damage; karma penalty: " + penalty);
            }
            else
            {
                Log.Debug(attacker.UserId + " attacked " + target.UserId + " for " + damage + " damage; karma awarded: " + reward);
                if (attacker.IsTraitor())
                    reward /= 5;
                attacker.LiveKarma += reward;
                Instance.Players[attacker.UserId] = attacker;
            }
            return damage;
        }

        [PluginEvent(ServerEventType.PlayerDeath)]
        public void PlayerDie(PlayerDeathEvent args)
        {
            if (args.Attacker == null || args.Attacker == args.Player)
                return; 

            if (!Instance.Players.TryGetValue(args.Attacker.UserId, out PlayerInfo killerInfo) || !Instance.Players.TryGetValue(args.Player.UserId, out PlayerInfo targetInfo))
            {
                Log.Warning("Killer or Target is not registered in the game. This shouldn't happen.");
                return;
            }

            // Update kills/deaths 
            if (!args.Attacker.DoNotTrack)
            {
                killerInfo.Kills++;
            }
            if (!args.Player.DoNotTrack)
            {
                targetInfo.Deaths++;
            }

            float penalty = Karma.KillPenalty(killerInfo);
            float reward = Karma.KillReward();
            bool friendlyFire = killerInfo.IsTraitor() == targetInfo.IsTraitor();
            bool karmaChanged = friendlyFire || (!killerInfo.IsTraitor() && targetInfo.IsTraitor());

            targetInfo.Hud.ShowNote($"You were killed by {args.Attacker.Nickname}" +
                $" they were a " +
                $"{(killerInfo.Role == TttRole.Spectator ? killerInfo.LastRole : killerInfo.Role)}.",3);

            killerInfo.Hud.ShowNote($"You killed {args.Player.Nickname}, they were a {targetInfo.LastRole}.",3);

            //Log.Info("Player {VictimName} ({VictimUserId}), a(n) {VictimRole}" +
            //    " was killed by {AttackerName} ({AttackerUserId}), a(n) {AttackerRole}. " +
            //    (karmaChanged ? "They " + (friendlyFire ? "lost" : "gained") + " {KarmaChange} karma." : ""),
            //    args.Player.Nickname, args.Player.UserId, targetInfo.Role, args.Attacker.Nickname, args.Attacker.UserId, killerInfo.Role, friendlyFire ? (int)penalty : (int)reward);
                
            if (killerInfo.IsTraitor() == targetInfo.IsTraitor())
            {
                penalty = Karma.KillPenalty(killerInfo);
                killerInfo.LiveKarma -= penalty;
                killerInfo.CleanRound = false;
            }
            else if (!killerInfo.IsTraitor() && targetInfo.IsTraitor())
            {
                reward = Karma.KillReward();
                killerInfo.LiveKarma += reward;
            }

            if (killerInfo.IsTraitor())
            {
                // 30xp for Detective
                // 5xp for Innocent kills
                // 0xp for Traitor kills.
                if (targetInfo.IsInnocent())
                {
                    killerInfo.Xp.AddXp(ScpTttConstants.XP_TRAITOR_KILL_INNOCENT,ScpTttConstants.MESSAGE_TRAITOR_KILL_INNOCENT);
                }
                else if(targetInfo.IsDetective()){
                    killerInfo.Xp.AddXp(ScpTttConstants.XP_TRAITOR_KILL_DETECTIVE, ScpTttConstants.MESSAGE_TRAITOR_KILL_DETECTIVE);
                }
            }

            // Save info
            Instance.Players[killerInfo.UserId] = killerInfo;
            Instance.Players[targetInfo.UserId] = targetInfo;
        }

    }
}
