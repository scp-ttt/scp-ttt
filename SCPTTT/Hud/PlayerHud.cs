using SCPTTT.Game;
using System.Collections.Generic;
using System.Text;
using SCPTTT.Util;
using System.Linq;
using System;
using PluginAPI.Core;
using PlayerInfo = SCPTTT.Game.PlayerInfo;

namespace SCPTTT.Hud
{
    public class PlayerHud
    {
        public PlayerInfo Player { get; private set; }
        private TttRound _round;
        internal bool ShouldUpdate = true;
        private List<HudNote> _notes = new List<HudNote>(); 

        public string DisplayString => CalculateDisplay();

        public void Setup(PlayerInfo player, TttRound round)
        {
            Player = player;
            _round = round;
        }

        public void ShowNote(string note, float duration = 2)
        {
            _notes.Add(new HudNote() { RemainingDuration = duration, Text = note });
        }

        internal void TickTimer()
        {
            foreach (HudNote note in _notes)
            {
                note.RemainingDuration -= 1;
            }
            _notes.RemoveAll(x => x.RemainingDuration <= 0);
        }
        
        private string CalculateDisplay()
        {
            if (_round == null || !ShouldUpdate)
                return null;
            StringBuilder sb = new StringBuilder();
            switch (_round.Status)
            {
                case RoundStatus.PreRound:
                    sb.Append("Pre-round!");
                    break;
                case RoundStatus.PostRound:
                    return ShowVictorText();
                default:
                    sb.Append($"<b><color={Player.Role.ToColor().AsTMColor()}>{Player.Role}</color></b>");
                    break;
            }
            sb.Append(" - ");
            sb.AppendLine(TimeSpan.FromSeconds(_round.Timer).ToString());
            List<PlayerInfo> traitors = ScpTtt.Instance.Players.AllTraitors();
            if (Player.Role == TttRole.Traitor && traitors.Count > 1)
                sb.AppendLine(FellowTraitors());

            foreach(HudNote note in _notes)
            {
                sb.AppendLine($"\"{note.Text}\"");
            }
            int newlines = sb.ToString().Count(x => x == '\n');
            sb.Append(HintUtilities.AlignAtLine(newlines));
            return sb.ToString();
        }

        private string FellowTraitors()
        {
            StringBuilder sb = new StringBuilder();
            if (_round.Status == RoundStatus.Ongoing)
            {
                List<PlayerInfo> traitors = ScpTtt.Instance.Players.AllTraitors();
                traitors.Remove(Player);
                if (Player.Role == TttRole.Traitor && traitors.Count > 0)
                {
                    sb.AppendLine("Your fellow traitors are: ");
                    foreach (var player in traitors)
                    {
                        sb.Append("<color=red><b>");
                        Player p = player.UserId.CreatePlayer();
                        sb.Append(p.Nickname);
                        sb.Append(" - ");
                        sb.Append("</b></color>");
                    }
                    sb.Length -= 3;
                }
            }
            return sb.ToString();
        }

        private string ShowVictorText()
        {
            StringBuilder sb = new StringBuilder();
            foreach (HudNote note in _notes)
            {
                sb.AppendLine($"\"{note.Text}\"");
            }
            int newlines = sb.ToString().Count(x => x == '\n');
            int height = 8 - newlines;
            sb.Append(new string('\n',height));
            sb.AppendLine("<align=center><size=60>Round Over!</size>");
            sb.AppendLine($"<size=60><color={_round.Victor?.ToColor().AsTMColor()}>{_round.Victor}s</color></size>");
            sb.AppendLine("<size=60>win!</size></align>");
            sb.Append(HintUtilities.AlignAtLine(13));
            return sb.ToString(); 
        }

    }
}
