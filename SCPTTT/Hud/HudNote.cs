﻿using System;
using System.Collections.Generic;

namespace SCPTTT.Hud
{
    public class HudNote : IEquatable<HudNote>
    {

        public string Text { get; set; }

        public float RemainingDuration { get; set; }

        public override bool Equals(object obj)
        {
            return obj is HudNote note && Equals(note);
        }

        public bool Equals(HudNote other)
        {
            return Text == other.Text && RemainingDuration == other.RemainingDuration;
        }

        public override int GetHashCode()
        {
            int hashCode = 2108821349;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Text);
            hashCode = hashCode * -1521134295 + RemainingDuration.GetHashCode();
            return hashCode;
        }
    }
}
