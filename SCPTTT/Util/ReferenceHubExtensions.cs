﻿using PluginAPI.Core;
using System.Linq;

namespace SCPTTT.Util
{
    public static class ReferenceHubExtensions
    {
        public static ReferenceHub ReferenceHub(this string userid) => 
            global::ReferenceHub.AllHubs.FirstOrDefault(x => x.characterClassManager.UserId == userid);

        public static Player CreatePlayer(this string userid) => Player.Get(userid);

    }
}
