﻿using Hints;
using System.Text;

namespace SCPTTT.Util
{
    public static class HintUtilities
    {

        public static Hint CreateTextHint(string text, float fadein = 0.5f)
        {
            Hint h = new TextHint(text, new HintParameter[] { new StringHintParameter("") }, HintEffectPresets.FadeInAndOut(fadein, 1f, 0f), 10f);
            return h;
        }


        // ;(
        /// <summary>
        /// If appended to the end of a string hint, will place the hint at the line specified by
        /// <paramref name="lineNumber"/> vertically.
        /// </summary>
        /// <param name="lineNumber">The line number to offset the text hint to.</param>
        /// <returns></returns>
        public static string AlignAtLine(int lineNumber)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 32; i > lineNumber; i--)
            {
                sb.Append("\n");
            }
            return sb.ToString();
        }

    }
}
