﻿using InventorySystem;
using InventorySystem.Items;
using InventorySystem.Items.Firearms;
using InventorySystem.Items.Firearms.Attachments;
using InventorySystem.Items.Pickups;
using MapGeneration.Distributors;
using UnityEngine;

namespace SCPTTT.Util
{
    public static class PickupFactory
    {
        private static Inventory hostInv;

        /// <summary>
        /// Creates an item on the server and syncs to clients.
        /// </summary>
        /// <param name="item">The <see cref="ItemType"/> to spawn.</param>
        /// <param name="pos">The position to place the object.</param>
        /// <returns>An initialized <see cref="Pickup"/> object.</returns>
        public static void GeneratePickup(ItemType item, Vector3 pos)
        {
            if (hostInv == null)
                hostInv = ReferenceHub.HostHub.inventory;

            if (!InventoryItemLoader.AvailableItems.TryGetValue(item, out ItemBase baseItem))
                return;

            ItemPickupBase pickup = Object.Instantiate(baseItem.PickupDropModel, pos, Quaternion.identity);
            pickup.Info.ItemId = item;
            pickup.Info.WeightKg = baseItem.Weight;

            if (pickup is FirearmPickup firearm)
            {
                firearm.Status = new FirearmStatus(0, FirearmStatusFlags.MagazineInserted, AttachmentsUtils.GetRandomAttachmentsCode(item));
                firearm.Distributed = true;
            }

            ItemDistributor.SpawnPickup(pickup);
        }

        /// <summary>
        /// Runs <see cref="GeneratePickup(ItemType, Vector3)"/> <paramref name="count"/> amount of times.
        /// </summary>
        /// <param name="item">The <see cref="ItemType"/> to spawn.</param>
        /// <param name="pos">The position to place the object.</param>
        /// <param name="count">The amount of objects to place at <paramref name="pos"/>.</param>
        /// <returns>An array of <see cref="Pickup"/>s that have been spawned.</returns>
        public static void GenerateMultiple(ItemType item, Vector3 pos, int count)
        {
            for (int i = 0; i < count; i++)
            {
                GeneratePickup(item, pos);
            }
        }

    }
}
