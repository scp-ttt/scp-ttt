﻿using InventorySystem;
using InventorySystem.Items;
using InventorySystem.Items.Armor;
using PluginAPI.Core;
using System.Collections.Generic;

namespace SCPTTT.Util
{
    public static class InventoryUtil
    {

        /// <summary>
        /// Replaces the current armor with new armor.
        /// </summary>
        /// <param name="inv"></param>
        /// <param name="bodyArmor"></param>
        /// <param name="preventExcessDrop"></param>
        public static void ReplaceArmor(this Inventory inv, ItemType bodyArmor, bool preventExcessDrop = true)
        {
            if (!bodyArmor.IsArmor() || bodyArmor != ItemType.None)
            {
                return;
            }

            if (inv.TryGetBodyArmor(out BodyArmor armor)){
                armor.DontRemoveExcessOnDrop = preventExcessDrop;
                inv.ServerDropItem(armor.ItemSerial);
            }
            BodyArmor newArmor = inv.ServerAddItem(bodyArmor) as BodyArmor;
            BodyArmorUtils.RemoveEverythingExceedingLimits(inv, newArmor);
        }

        public static bool IsArmor(this ItemType type)
        {
            Log.Debug("Current itemtype: " + type + " - " + type.ToString()+" - result: "+ type.ToString().Contains("Armor"));
            return type.ToString().Contains("Armor");
        }
    }
}
