﻿using SCPTTT.Game;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SCPTTT.Util
{
    public static class TttRoleExtensions
    {

        public static Color ToColor(this TttRole role)
        {
            switch(role)
            {
                case TttRole.Detective:
                    return Color.blue;
                case TttRole.Innocent:
                    return Color.green;
                case TttRole.Traitor:
                    return Color.red;
                default:
                    return Color.white;
            }
        }

        public static string AsTMColor(this Color c)
        {
            Color32 color = new Color32((byte)(c.r * 255), (byte)(c.g * 255), (byte)(c.b * 255), 255);
            return "#" + color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
        }

        public static bool IsTraitor(this PlayerInfo player) => player.Role == TttRole.Traitor;

        public static bool IsInnocent(this PlayerInfo player) => player.Role == TttRole.Innocent;

        public static bool IsDetective(this PlayerInfo player) => player.Role == TttRole.Detective;

        public static List<PlayerInfo> AllTraitors(this Dictionary<string,PlayerInfo> playerList)
        {
            return playerList.Values.Where(x => x.IsTraitor()).ToList();
        }

    }
}
