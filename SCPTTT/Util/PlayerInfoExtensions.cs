﻿using PluginAPI.Core;
using SCPTTT.Game;
using System.Text;
using PlayerInfo = SCPTTT.Game.PlayerInfo;

namespace SCPTTT.Util
{
    public static class PlayerInfoExtensions
    {
        public static void UpdateCustomInfo(this PlayerInfo player)
        {
            Player ply = player.UserId.CreatePlayer();
            StringBuilder sb = new StringBuilder();
            sb.Append("<color=");
            sb.Append(HealthColors.GetHealthColor(ply.Health));
            sb.Append(">");
            sb.Append(HealthColors.StateFromHp(ply.Health).Format());
            sb.Append("</color>\n");
            if (player.IsDetective())
                sb.Append("<color=cyan>Detective</color>\n");
            ply.CustomInfo = sb.ToString();
        }

    }
}
