﻿using SCPTTT.Game;
using SCPTTT.Game.Items;

namespace SCPTTT.Util
{
    public static class EnumExtensions
    {
        /// <summary>
        /// Returns whether or not the flaggable enum contains the given role.
        /// </summary>
        /// <param name="role"></param>
        /// <param name="checkRole"></param>
        /// <returns></returns>
        public static bool FlagContains(this BuyableRole role, BuyableRole checkRole)
        {
            return (role & checkRole) == checkRole;    
        }

        public static bool ContainsGameRole(this BuyableRole role, TttRole gameRole)
        {
            BuyableRole gameRoleAsBuyable;
            switch (gameRole)
            {
                case TttRole.Traitor:
                    gameRoleAsBuyable = BuyableRole.Traitor;
                    break;
                case TttRole.Innocent:
                default:
                    gameRoleAsBuyable = BuyableRole.Innocent;
                    break;
                case TttRole.Detective:
                    gameRoleAsBuyable = BuyableRole.Detective;
                    break;
            }
            return role.FlagContains(gameRoleAsBuyable);
        }
    }
}
