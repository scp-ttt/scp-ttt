﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace SCPTTT.Util
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Shuffle<T>(
            this IEnumerable<T> source)
        {
            using (RNGCryptoServiceProvider rngScp = new RNGCryptoServiceProvider())
            {
                var buffer = source.ToList();
                for (int i = 0; i < buffer.Count; i++)
                {
                    int j = rngScp.Next(i, buffer.Count);
                    yield return buffer[j];

                    buffer[j] = buffer[i];
                }
            }
        }

        private static double NextDouble(this RNGCryptoServiceProvider rng)
        {
            var data = new byte[sizeof(uint)];
            rng.GetBytes(data);
            var randUint = BitConverter.ToUInt32(data, 0);
            return randUint / (uint.MaxValue + 1.0);
        }

        private static int Next(this RNGCryptoServiceProvider rng, int minValue, int maxValue)
        {
            if (minValue > maxValue)
            {
                throw new ArgumentOutOfRangeException();
            }
            return (int)Math.Floor((minValue + ((double)maxValue - minValue) * rng.NextDouble()));
        }
    }
}
