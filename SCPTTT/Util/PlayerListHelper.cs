﻿using PluginAPI.Core;
using SCPTTT.Game;
using System;
using System.Text;
using PlayerInfo = SCPTTT.Game.PlayerInfo;

namespace SCPTTT.Util
{
    public static class PlayerListHelper
    {

        public static string CreatePlayerlistText(PlayerInfo info, Player p) 
        {
            StringBuilder sb = new StringBuilder();
            //if (ScpTtt.Instance.CurrentRound != null)
            //{
            //    if (p.Role == RoleType.Spectator || p.Role == RoleType.None)
            //        sb.Append(HealthState.Dead.Format());
            //    else
            //        sb.Append(HealthColors.StateFromHp(p.Health).Format());
            //    sb.Append(" - ");
            //}
            sb.Append($"Level {info.Xp.Level} - ");
            sb.Append($"{Math.Round(info.Karma)} Karma");
            UserGroup group = ServerStatic.GetPermissionsHandler().GetUserGroup(p.UserId);
            string srvRank = group == null ? null : group.BadgeText;
            if (!string.IsNullOrEmpty(srvRank))
            {
                sb.Append($" ({srvRank})");
            }
            return sb.ToString();
        }

        public static string ParsePlayerName(PlayerInfo info, Player p)
        {
            StringBuilder sb = new StringBuilder();
            TttRound currentRound = ScpTtt.Instance.CurrentRound;
            if (currentRound != null && currentRound.Status == RoundStatus.Ongoing && info.Role == TttRole.Detective)
            {
                sb.Append($"[D] ");
            }
            sb.Append(p.ReferenceHub.nicknameSync.MyNick);
            return sb.ToString();
        }

        /// <summary>
        /// Runs <see cref="ParsePlayerName(PlayerInfo, Player)"/> and <see cref="CreatePlayerlistText(PlayerInfo, Player)"/>
        /// </summary>
        /// <param name="info"></param>
        /// <param name="p"></param>
        public static void UpdatePlayerIdentity(PlayerInfo info, Player p)
        {
            string text = CreatePlayerlistText(info, p);
            p.ReferenceHub.serverRoles.Network_myText = text;
            //if (ScpTtt.Instance.CurrentRound != null)
            //{
            //    p.ReferenceHub.serverRoles.NetworkMyColor = (p.Role == RoleType.None || p.Role == RoleType.Spectator ? "carmine" : HealthColors.GetHealthColor(p.Health));
            //}
            p.ReferenceHub.nicknameSync.Network_displayName = ParsePlayerName(info, p);

        }

    }
}
